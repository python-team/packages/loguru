loguru (0.7.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - Fix tests with Python 3.13 (closes: #1088751).
  * Drop an unused Lintian override.
  * Make some Lintian overrides slightly less specific.

 -- Colin Watson <cjwatson@debian.org>  Sun, 08 Dec 2024 15:56:29 +0000

loguru (0.7.2-3) unstable; urgency=medium

  * Team upload.
  * Add missing Test-Depends: python3-mypy

 -- Andreas Tille <tille@debian.org>  Fri, 05 Jan 2024 17:04:28 +0100

loguru (0.7.2-2) unstable; urgency=medium

  * Team upload.
  * Add missing Test-Depends: python3-freezegun

 -- Andreas Tille <tille@debian.org>  Fri, 05 Jan 2024 08:54:44 +0100

loguru (0.7.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #1056421
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Drop useless override_dh_auto_test
  * Build-Depends: python3-freezegun, python3-mypy

 -- Andreas Tille <tille@debian.org>  Mon, 11 Dec 2023 09:05:44 +0100

loguru (0.6.0-3) unstable; urgency=medium

  * Update python3.11 patch (Closes: #1024955)

 -- Nilesh Patra <nilesh@debian.org>  Sat, 10 Dec 2022 13:07:10 +0530

loguru (0.6.0-2) unstable; urgency=medium

  * Cherry pick upstream commit to fix
    python 3.11 FTBFS (Closes: #1017233, #1017483)
  * Bump Standards-Version to 4.6.1 (no changes needed)
  * Add d/s/lintian-overrides for FP

 -- Nilesh Patra <nilesh@debian.org>  Thu, 25 Aug 2022 18:02:17 +0530

loguru (0.6.0-1) unstable; urgency=medium

  * Add myself to uploaders
  * New upstream version 0.6.0
  * Drop patches cherry-picked from upstream
  * Refresh test_time_rotation_reopening_native.patch
  * d/rules: Do not remove test_exceptions_formatting.py
    + Seems to be fixed in this release
  * Add autopkgtests

 -- Nilesh Patra <nilesh@debian.org>  Sun, 13 Feb 2022 20:33:47 +0530

loguru (0.5.3-5) unstable; urgency=medium

  * Team Upload.
  * Add patch to fix code injection vulnerability (Closes: #1004194)
    + Fixes CVE: CVE-2022-0329

 -- Nilesh Patra <nilesh@debian.org>  Wed, 26 Jan 2022 02:06:39 +0530

loguru (0.5.3-4) unstable; urgency=medium

  * Team Upload.
  [ Debian Janitor ]
  * Use canonical URL in Vcs-Git.

  [ Nilesh Patra ]
  * Backport upstream patch to fix FTBFS (Closes: #997471)
  * Bump Standards-Version to 4.6.0 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 31 Oct 2021 02:15:29 +0530

loguru (0.5.3-3) unstable; urgency=medium

  * Team Upload.
  * Disable test_time_rotation_reopening_native

 -- Nilesh Patra <nilesh@debian.org>  Fri, 30 Apr 2021 00:39:47 +0530

loguru (0.5.3-2) unstable; urgency=medium

  * Team Upload.
  * Add upstream/metadata
  * d/control: Minor fix in description
  * d/rules: do not pipe dh_auto_test with true,
    rather remove the faulty test
  * d/rules: respect nocheck for build time tests

 -- Nilesh Patra <nilesh@debian.org>  Thu, 29 Apr 2021 23:56:03 +0530

loguru (0.5.3-1) unstable; urgency=medium

  * Initial release (Closes: #983262)

 -- Steffen Moeller <moeller@debian.org>  Sun, 21 Feb 2021 17:22:03 +0100
